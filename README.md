# imgur plugin (2.8) for MyBB 1.8

This plugin adds a drag & drop zone in the new post and full edit interface
The pictures are uploaded and the corresponding MyCode is inserted in the message

The resulting url is inserted in the post.

# REQUIREMENT
Just declare a new app in imgur, set the Client ID in
the settings (MyBB ACP) and run :)

# UPGRADE
**From 2.3 and above**: deactivate and reactivate the plugin.

**Before 2.3** : Uninstall the plugin, upload the content of the UPLOAD directory
into your forum root and install & activate the plugin.

__WARNING__ : DO NOT FORGET to save your ClienID before uninstalling the plugin.


# CHANGELOG
* **2.8** :
  * added an option to choose the place of the uploader
* **2.7** :
  * correction of button (comes to light when clicking)
  * added imgur uploader in signature (UCP), settable by admin (ACP)
* **2.6** :
  * added dark picture (used as button)
  * corrected the quick reply insertion
  * added PM quick reply button
  * added cursor style on button
* **2.5** :
  * The popup way now authorize multiple uploads in the same time
* **2.4** :
  * Just renamed the "dropfile" div to avoid incompatibility with some other plugins
* **2.3** :
  * Scheme suppressed in links, allowing both http and https.
* **2.2** :
  * Enable the plugin in Quick reply
* **2.1** :
  * Corrected a JS bug making some pictures uploaded several times
  * Reintroduction of the popup (modal) upload. When clicking on the "imgur", the modal appears. Drag&drop always working
  * Added a new setting allowing to create a link to the original picture when the displayed one is resized.
* **2.0** :
  * The button is now a zone where you can drag & drop the pictures to upload.
  * You can upload several pictures in one time.
